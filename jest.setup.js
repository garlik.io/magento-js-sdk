global.fetch = require("jest-fetch-mock");

import { MagentoClient, MagentoConnection } from "./src";
global.SDK = new MagentoClient();

import testGenerator from "./__test__/utils/testGenerator";
global.testGenerator = testGenerator;

import fixtures from "./__fixtures__";
global.fixtures = fixtures;
