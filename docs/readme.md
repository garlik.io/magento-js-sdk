# Magento SDK

To set the development environment you need to install Lando

### Lando on Mac

- Install Homebrew
  - `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
  - `brew install wget`
  - `brew upgrade && brew update && brew doctor && brew cask upgrade && brew cask doctor`
- Update Gems
  - `sudo gem update --system -n /usr/local/bin`
- Install Docker
  - `brew cask install docker`
- Install Lando
  - `brew cask install lando`

### Lando on Linux

- Remove old versions of Docker CE
  - `sudo apt-get remove docker docker-engine docker.io containerd runc`
- Update the software repositories
  - `sudo apt-get update`
- Install dependencies
  - `sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common`
- Add Docker repositories
  - `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
  - `sudo apt-key fingerprint 0EBFCD88`
  - `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`
- Install Docker CE
  - `sudo apt-get update`
  - `sudo apt-get install docker-ce docker-ce-cli containerd.io`
- Download Lando (latest version)
  - `https://github.com/lando/lando/releases`
- Install Lando using the software manager or by command line
- Set Docker permmisions
  - `sudo usermod -a -G docker $USER`

### Start Lando environment
After instaling Lando run `lando start` on the folder repository to set the development environment, if you already created the environment you must destroy it first (see Destroy Lando environment section)

### Run testing site
After staring the Lando environment open the link `http://magento-sdk.lndo.site` to run the testing site

### Destroy Lando environment
To dstroy the Lando environment run `lando destroy -y`

### Postman shared collection
Use this link to import the collection `https://www.getpostman.com/collections/5457b98858dbfa0d224c`, from the `Import option`, choose `Import From Link` copy the link and import the collection (every time changes are made to the collection this same link will be updated)

### Build and deploy package
Steps to build and deploy a new package
- Change the package version on the `package.json` file
- Run command to build the package
  - `npm run build`
- Run the command to deploy the new package
  - `npm publish`
