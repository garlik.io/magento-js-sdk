# Code Docs

Refer to: `docs/index.html`

# Example Code

```javascript
/**
 * Require the MagentoConnection and MagentoClient class
 */
const {MagentoConnection, MagentoClient} = require('./dist/node.magento.min');

/**
 * Create an admin connection so that we can take admin actions
 */
MagentoConnection.FromAdminAccount(
    {
        password: 'magento-admin-password',
        username: 'magento-admin-username',
    },

    /**
     * NOTE: if your Magento Swaggar docs are at www.magento-store.com/subdirectory/swaggar
     * the URL here should be https://www.magento-store.com/subdirectory/
     */
    'https://www.magento-store.com/' 
)

/**
 * Then, take the admin connection and create a Magento admin client with an optional default store.
 * This is helpful if we want to pull store-specific data on products and not default-admin scope data.
 */
.then(magentoAdminConnection => {
    const storeCode = 'us'
    return new MagentoClient(magentoAdminConnection, storeCode);
})

/**
 * Then, take the admin connection and create a Magento admin client with an optional default store.
 * This is helpful if we want to pull store-specific data on products and not default-admin scope data.
 */
.then(adminClient => {
    const sku = 'M-FR001U';
    // Just for fun, let's switch to the French storeview before we fetch the product data.
    return adminClient.setDefaultStore('fr-ca').get(`V1/products/${sku}`)
        .then(({ data: product }) => {
            const productNameInFrench = product.name;
            return console.log(productNameInFrench);
        });
})
.catch((error) => {
    return console.log(error);
});

```

