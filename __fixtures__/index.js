const filesystem = require('fs');

const getAllFilesFromFolder = (directory) => {
  let results = [];

  filesystem.readdirSync(directory).forEach((file) => {
    if (file !== 'index.js') {
      const filePath = directory + '/' + file;
      const stat = filesystem.statSync(filePath);

      if (stat && stat.isDirectory()) {
        results = results.concat(getAllFilesFromFolder(filePath));
      } else {
        results.push(filePath);
      }
    }
  });

  return results;
};

const filesPaths = getAllFilesFromFolder(__dirname);

const filesToExport = {};

filesPaths.forEach(path => {
  const splittedPath = path.split('/');
  const fileName = splittedPath[splittedPath.length - 1].replace('Fixture.js', '');
  filesToExport[fileName] = require(path);
});

export default filesToExport;
