const getResponse = {
  resource: [
    {
      id: 1,
      question: 'Are you currently using Magento Products?',
      answer_type: 'SINGLE',
      enable: true,
      survey_id: 1
    },
    {
      id: 2,
      question: 'How many locations does your business has?',
      answer_type: 'SINGLE',
      enable: true,
      survey_id: 1
    },
    {
      id: 3,
      question: 'How would you like to be contacted?',
      answer_type: 'MULTIPLE',
      enable: true,
      survey_id: 1
    }
  ]
};

//json data in brackets
const findResponse = {
  id: 1,
  question: 'Are you currently using Magento Products?',
  answer_type: 'SINGLE',
  enable: true,
  survey_id: 1
};

//dreamfactory json data structure
const createResponse = { resource: [{ id: 1 }] };
const updateResponse = { resource: [{ id: 1 }] };
const deleteResponse = { resource: [{ id: 1 }] };

module.exports = {
  get: JSON.stringify(getResponse),
  create: JSON.stringify(createResponse),
  find: JSON.stringify(findResponse),
  update: JSON.stringify(updateResponse),
  delete: JSON.stringify(deleteResponse)
};
