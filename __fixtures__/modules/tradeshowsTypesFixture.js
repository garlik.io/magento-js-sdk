const getResponse = {
  resource: [
    { id: 1, name: "Corporate" },
    { id: 2, name: "Regional" }
  ]
};

const findResponse = { id: 2, name: "Regional" };

const createResponse = { resource: [{ id: 2 }] };
const updateResponse = { resource: [{ id: 2 }] };
const deleteResponse = { resource: [{ id: 2 }] };

module.exports = {
  get: JSON.stringify(getResponse),
  create: JSON.stringify(createResponse),
  find: JSON.stringify(findResponse),
  update: JSON.stringify(updateResponse),
  delete: JSON.stringify(deleteResponse)
};