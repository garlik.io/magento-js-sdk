//data inside resource
const getResponse = {
  resource: [
    {
      id: 1,
      url: 'www.google.com',
      ocr_result: 'active',
      created_at: '2019-12-02 20:32:15',
      updated_at: '2019-12-02 20:32:15'
    },
    {
      id: 2,
      url: 'www.amazon.com',
      ocr_result: 'active',
      created_at: '2019-12-02 20:32:47',
      updated_at: '2019-12-02 20:32:47'
    },
    {
      id: 3,
      url: 'www.testsite.com',
      ocr_result: 'active',
      created_at: '2019-12-02 20:32:55',
      updated_at: '2019-12-02 20:32:55'
    },
    {
      id: 4,
      url: 'www.clients.com',
      ocr_result: 'active',
      created_at: '2019-12-02 20:33:04',
      updated_at: '2019-12-02 20:33:04'
    }
  ]
};
 
//json data in brackets
const findResponse = {
  id: 1,
  url: 'www.google.com',
  ocr_result: 'active',
  created_at: '2019-12-02 20:32:15',
  updated_at: '2019-12-02 20:32:15'
};
 
//dreamfactory json data structure
const createResponse = { resource: [{ id: 1 }] };
const updateResponse = { resource: [{ id: 1 }] };
const deleteResponse = { resource: [{ id: 1 }] };
 
module.exports = {
  get: JSON.stringify(getResponse),
  create: JSON.stringify(createResponse),
  find: JSON.stringify(findResponse),
  update: JSON.stringify(updateResponse),
  delete: JSON.stringify(deleteResponse)
};