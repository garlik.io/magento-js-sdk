//data inside resource
const getResponse = {
  resource: [
    {
      id: 1,
      company_name: 'company number 1',
      notes: 'this is a note.',
      tradeshow_id: 1,
      lead_survey_id: 1,
      order_id: 1,
      info_id: 1,
      lead_type_id: 2,
      user_id: 1
    },
    {
      id: 2,
      company_name: 'Company number 2',
      notes: 'this is a note.',
      tradeshow_id: 1,
      lead_survey_id: 1,
      order_id: 1,
      info_id: 1,
      lead_type_id: 2,
      user_id: 1
    },
    {
      id: 3,
      company_name: 'Company number 3',
      notes: 'this is a note.',
      tradeshow_id: 1,
      lead_survey_id: 1,
      order_id: 1,
      info_id: 1,
      lead_type_id: 2,
      user_id: 1
    }
  ]
};

//json data in brackets
const findResponse = {
  id: 1,
  company_name: 'company number 1',
  notes: 'this is a note.',
  tradeshow_id: 1,
  lead_survey_id: 1,
  order_id: 1,
  info_id: 1,
  lead_type_id: 2,
  user_id: 1
};

//dreamfactory json data structure
const createResponse = { resource: [{ id: 1 }] };
const updateResponse = { resource: [{ id: 1 }] };
const deleteResponse = { resource: [{ id: 1 }] };

module.exports = {
  get: JSON.stringify(getResponse),
  create: JSON.stringify(createResponse),
  find: JSON.stringify(findResponse),
  update: JSON.stringify(updateResponse),
  delete: JSON.stringify(deleteResponse)
};