const getResponse = {
  resource: [
    {
      bundle: "New created from SDK method",
      created_at: "2019-11-27 23:04:56",
      updated_at: "2019-11-27 23:04:56",
      id: 139,
      amount: 0
    },
    {
      bundle: "New created from SDK method",
      created_at: "2019-11-27 23:52:08",
      updated_at: "2019-11-27 23:52:08",
      id: 140,
      amount: 0
    },
    {
      bundle: "New created from SDK method",
      created_at: "2019-11-27 23:52:40",
      updated_at: "2019-11-27 23:52:40",
      id: 141,
      amount: 0
    }
  ]
};

const findResponse = {
  bundle: "New bundle from Jest",
  created_at: "2019-11-28 19:20:58",
  updated_at: "2019-11-28 19:20:58",
  id: 143,
  amount: 5
};

const createResponse = { resource: [{ id: 143 }] };
const updateResponse = { resource: [{ id: 143 }] };
const deleteResponse = { resource: [{ id: 143 }] };

module.exports = {
  create: JSON.stringify(createResponse),
  delete: JSON.stringify(deleteResponse),
  find: JSON.stringify(findResponse),
  get: JSON.stringify(getResponse),
  update: JSON.stringify(updateResponse)
};
