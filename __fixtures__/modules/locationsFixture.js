const getResponse = {
  resource: [
    {
      id: 1,
      address_line: "A place in the world",
      city: "Los Angeles",
      zip_code: "12345",
      other_country: null,
      other_state: null,
      state_id: 2
    },
    {
      id: 2,
      address_line: "The direction of the event",
      city: "Miami",
      zip_code: "12344",
      other_country: null,
      other_state: null,
      state_id: 34
    },
    {
      id: 3,
      address_line: "Another Place around the world",
      city: "Manhattan",
      zip_code: "12332",
      other_country: null,
      other_state: null,
      state_id: 37
    }
  ]
};

const findResponse = {
  id: 1,
  address_line: "A place in the world",
  city: "Los Angeles",
  zip_code: "12345",
  other_country: null,
  other_state: null,
  state_id: 2
};

const createResponse = { resource: [{ id: 1 }] };
const updateResponse = { resource: [{ id: 1 }] };
const deleteResponse = { resource: [{ id: 1 }] };

module.exports = {
  create: JSON.stringify(createResponse),
  delete: JSON.stringify(deleteResponse),
  find: JSON.stringify(findResponse),
  get: JSON.stringify(getResponse),
  update: JSON.stringify(updateResponse)
};
