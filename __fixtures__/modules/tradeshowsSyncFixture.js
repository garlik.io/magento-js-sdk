const getResponse = {
  resource: [
    {
      id: 1,
      uuid: "1",
      is_uploaded:  "1",
      tradeshow_id: 1
    }
  ]
};

const findResponse = {
  id: 1,
  uuid: "",
  is_uploaded: "",
  tradeshow_id: 1
};

const createResponse = { resource: [{ id: 1 }] };
const updateResponse = { resource: [{ id: 1 }] };
const deleteResponse = { resource: [{ id: 1 }] };

module.exports = {
  create: JSON.stringify(createResponse),
  delete: JSON.stringify(deleteResponse),
  find: JSON.stringify(findResponse),
  get: JSON.stringify(getResponse),
  update: JSON.stringify(updateResponse)
};
