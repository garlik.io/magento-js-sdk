const getResponse = {
    resource: [
        {
            "id": 1,
            "first_name": "Sharon",
            "last_name": "Denning",
            "email": "chad_feene6@gmail.com",
            "phone": " 214-238-2014",
            "location_id": 1
        },
        {
            "id": 2,
            "first_name": "Mary",
            "last_name": "Ponce",
            "email": "ana_bayer1978@hotmail.com",
            "phone": " 320-201-9988",
            "location_id": 4
        },
        {
            "id": 3,
            "first_name": "Mark",
            "last_name": "Hardy",
            "email": "zelda2015@hotmail.com",
            "phone": "972-754-3610",
            "location_id": 5
        }
    ]
};


const findResponse = {
    id: 2,
    first_name: "Mary",
    last_name: "Ponce",
    email: "ana_bayer1978@hotmail.com",
    phone: "320-201-9988",
    location_id: 4
};

const createResponse = { resource: [{ id: 6 }] };
const updateResponse = { resource: [{ id: 6 }] };
const deleteResponse = { resource: [{ id: 6 }] };

module.exports = {
    create: JSON.stringify(createResponse),
    delete: JSON.stringify(deleteResponse),
    find: JSON.stringify(findResponse),
    get: JSON.stringify(getResponse),
    update: JSON.stringify(updateResponse)
};
