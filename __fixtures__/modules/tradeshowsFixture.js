const getResponse = {
  resource: [
    {
      id: 1,
      name: "Sysco Boston Spring Rhode Island Food Show",
      description: null,
      start_date: "2019-12-10 00:00:00",
      end_date: "2019-12-13 00:00:00",
      ctl_downloads: 0,
      ctl_uploads: 0,
      created_by: null,
      created_at: "2019-11-27 17:48:59",
      updated_at: "2019-11-27 19:37:00",
      location_id: 1,
      tradeshows_category_id: 1,
      tradeshows_type_id: 1
    },
    {
      id: 2,
      name: "United Fresh Produce Association Show",
      description: null,
      start_date: "2019-12-07 15:00:00",
      end_date: "2019-12-07 20:00:00",
      ctl_downloads: 0,
      ctl_uploads: 0,
      created_by: null,
      created_at: "2019-11-27 18:05:51",
      updated_at: "2019-11-27 18:05:51",
      location_id: 2,
      tradeshows_category_id: 2,
      tradeshows_type_id: 1
    },
    {
      id: 3,
      name: "Summer Fancy Food Show",
      description: null,
      start_date: "2020-01-05 10:00:00",
      end_date: "2020-01-07 17:00:00",
      ctl_downloads: 0,
      ctl_uploads: 0,
      created_by: null,
      created_at: "2019-11-27 18:07:46",
      updated_at: "2019-11-27 18:07:46",
      location_id: 3,
      tradeshows_category_id: 1,
      tradeshows_type_id: 1
    }
  ]
};

const findResponse = {
  id: 1,
  name: "Sysco Boston Spring Rhode Island Food Show",
  description: null,
  start_date: "2019-12-10 00:00:00",
  end_date: "2019-12-13 00:00:00",
  ctl_downloads: 0,
  ctl_uploads: 0,
  created_by: null,
  created_at: "2019-11-27 17:48:59",
  updated_at: "2019-11-27 19:37:00",
  location_id: 1,
  tradeshows_category_id: 1,
  tradeshows_type_id: 1
};

const createResponse = { resource: [{ id: 7 }] };
const updateResponse = { resource: [{ id: 7 }] };
const deleteResponse = { resource: [{ id: 7 }] };

module.exports = {
  get: JSON.stringify(getResponse),
  create: JSON.stringify(createResponse),
  find: JSON.stringify(findResponse),
  update: JSON.stringify(updateResponse),
  delete: JSON.stringify(deleteResponse)
};
