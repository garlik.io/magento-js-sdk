const getDocIds = {
  resource: [
    {
      documents_id: 1,
      document_tags_id: 1
    },
    {
      documents_id: 1,
      document_tags_id: 2
    },
    {
      documents_id: 2,
      document_tags_id: 3
    },
    {
      documents_id: 3,
      document_tags_id: 2
    }
  ]
};

const getIdTag = {id: 1};

module.exports = {
  getAll: JSON.stringify(getDocIds),
  getSingle: JSON.stringify(getIdTag)
}