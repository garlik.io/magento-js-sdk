const getResponse = {
  resource: [
    {
      id: 1,
      created_at: '2019-11-20 00:46:02',
      updated_at: '2019-11-20 00:46:02',
      survey_id: 2
    },
    {
      id: 2,
      survey_id: 1
    },
    {
      id: 3,
      created_at: '2019-12-02 23:36:40',
      updated_at: '2019-12-02 23:36:40',
      survey_id: 1
    }
  ]
};

const findResponse = {
  id: 1,
  created_at: '2019-11-20 00:46:02',
  updated_at: '2019-11-20 00:46:02',
  survey_id: 2
};

const createResponse = { resource: [{ id: 1 }] };
const updateResponse = { resource: [{ id: 1 }] };
const deleteResponse = { resource: [{ id: 1 }] };

module.exports = {
  get: JSON.stringify(getResponse),
  create: JSON.stringify(createResponse),
  find: JSON.stringify(findResponse),
  update: JSON.stringify(updateResponse),
  delete: JSON.stringify(deleteResponse)
};