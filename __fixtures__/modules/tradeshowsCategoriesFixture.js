const getResponse = {
  resource: [
    { id: 2, name: "Not Ordering" },
    { id: 1, name: "Ordering" }
  ]
};

const findResponse = { id: 1, name: "Ordering" };

const createResponse = { resource: [{ id: 1 }] };
const updateResponse = { resource: [{ id: 1 }] };
const deleteResponse = { resource: [{ id: 1 }] };

module.exports = {
  get: JSON.stringify(getResponse),
  create: JSON.stringify(createResponse),
  find: JSON.stringify(findResponse),
  update: JSON.stringify(updateResponse),
  delete: JSON.stringify(deleteResponse)
};