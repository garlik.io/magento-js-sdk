const getResponse = {
  resource: [
    {
      id: 1,
      name: "United States",
      abbreviation: "US",
      enabled: true
    },
    {
      id: 2,
      name: "Costa Rica",
      abbreviation: "CR",
      enabled: true
    }
  ]
};

const findResponse = {
  id: 1,
  name: "United States",
  abbreviation: "US",
  enabled: true
};

const createResponse = { resource: [{ id: 1 }] };
const updateResponse = { resource: [{ id: 1 }] };
const deleteResponse = { resource: [{ id: 1 }] };

module.exports = {
  create: JSON.stringify(createResponse),
  delete: JSON.stringify(deleteResponse),
  find: JSON.stringify(findResponse),
  get: JSON.stringify(getResponse),
  update: JSON.stringify(updateResponse)
};
