const getResponse = {
  resource: [
    {
      name: "Apple",
      description: "IT company",
      enable: "true",
      logo_url: "https://example.com/apple",
      location_id: "3"
    },
    {

      name: "Dos Pinos",
      description: "Milk company",
      enable: true,
      logo_url: "https://example.com/pinos",
      location_id: 4
    },

    {

      name: "Disney",
      description: "Entertainment company",
      enable: true,
      logo_url: "https://example.com/disney",
      location_id: 3
    }

  ]
};


const findResponse = {
  "id": 2,
  "name": "Disney",
  "description": "Entertainment company",
  "enable": true,
  "logo_url": "https://example.com/disney",
  "location_id": 3
};



const createResponse = { resource: [{ id: 4 }] };
const updateResponse = { resource: [{ id: 4 }] };
const deleteResponse = { resource: [{ id: 4 }] };


module.exports = {
  create: JSON.stringify(createResponse),
  delete: JSON.stringify(deleteResponse),
  find: JSON.stringify(findResponse),
  get: JSON.stringify(getResponse),
  update: JSON.stringify(updateResponse)
};


