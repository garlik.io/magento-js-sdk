const getResponse = {
    resource: [
        {
            status: true,
            company_id: 2,
            info_id: 7,
            user_type_id: 1,
            email: "marlon.rojas@dnamicworld.com"
        },
        {
            status: true,
            company_id: 2,
            info_id: 7,
            user_type_id: 1,
            email: "jorge.paiz@dnamic.ai"
        },
        {
            status: true,
            company_id: 2,
            info_id: 7,
            user_type_id: 1,
            email: "eduardo.alfaro@dnamic.ai"
        }

    ]
};

const findResponse = {
    id: 6,
    status: true,
    company_id: 2,
    info_id: 7,
    user_type_id: 1,
    email: "melania.gonzalez@dnamic.ai"
};

const createResponse = { resource: [{ id: 40 }] };
const updateResponse = { resource: [{ id: 40 }] };
const deleteResponse = { resource: [{ id: 40 }] };


module.exports = {
    get: JSON.stringify(getResponse),
    create: JSON.stringify(createResponse),
    find: JSON.stringify(findResponse),
    update: JSON.stringify(updateResponse),
    delete: JSON.stringify(deleteResponse)
};








