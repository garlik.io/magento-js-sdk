const getResponse = {
    resource: [
      {
        name: "File number one",
        description: "This is an example of a document record.",
        url: "https://google.com"
      },
      {
        name: "File number two",
        description: "This is an example of a document record.",
        url: "https://google.com"
      },
      {
        name: "File number three",
        description: "This is an example of a document record.",
        url: "https://google.com"
      }
    ]
  };

  const findResponse = {
    id : 1,
    name: "File number one",
    description: "This is an example of a document record.",
    url: "https://google.com"
  };



const createResponse = { resource: [{ id: 119 }] };
const updateResponse = { resource: [{ id: 119 }] };
const deleteResponse = { resource: [{ id: 119 }] };


  module.exports = {
    create: JSON.stringify(createResponse),
    delete: JSON.stringify(deleteResponse),
    find: JSON.stringify(findResponse),
    get: JSON.stringify(getResponse),
    update: JSON.stringify(updateResponse)
  };
  