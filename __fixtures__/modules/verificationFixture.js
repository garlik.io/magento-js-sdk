const sendedUserCode = {
  message: 'code send',
  error: null,
  email: 'joseph.scafidi@dnamic.ai'
};

const validatedUserCode = {
  message: "code validated",
  error: null,
  code: {
    id: 159,
    email: "joseph.scafidi@dnamic.ai",
    code: "9297",
    created_at: "2019-12-09 21:15:28",
    expiration_time: 72
  }
};

const validatedCompanyCode = {
  message: 'code validated',
  error: null,
  company: {
    id: 1,
    name: 'Dos Pinos',
    description: 'Milk company',
    enable: true,
    logo_url: 'https://example.com/pinos',
    location_id: 4,
    code: '8436'
  }
};

module.exports = {
  receiveCode: JSON.stringify(sendedUserCode),
  validatedResponse: JSON.stringify(validatedUserCode),
  CompanyValidated: JSON.stringify(validatedCompanyCode)
};
