const getResponse = {
    resource: [
        {
            name: "Manuals",
            enable: true
        },
        {
            name: "Brochures",
            enable: true    
        },
        {
            name: "Demo",
            enable: true
        },
        {
            name: "Restaurants",
            enable: true
        }
    ]
  };

  const findResponse = {
    "id": 1,
    "name": "Manuals",
    "enable": true
  };



const createResponse = { resource: [{ id: 9 }] };
const updateResponse = { resource: [{ id: 9 }] };
const deleteResponse = { resource: [{ id: 9 }] };


  module.exports = {
    create: JSON.stringify(createResponse),
    delete: JSON.stringify(deleteResponse),
    find: JSON.stringify(findResponse),
    get: JSON.stringify(getResponse),
    update: JSON.stringify(updateResponse)
  };
  