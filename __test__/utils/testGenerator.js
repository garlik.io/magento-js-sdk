export default table => {
  return describe(`Test the CRUD methods for the table ${table.name.toUpperCase()}`, () => {
    beforeEach(() => {
      fetch.resetMocks();
    });

    if (
      !table.hasOwnProperty("crudMethods") ||
      table.crudMethods.includes("get")
    ) {
      it(`Get method: get all items from table ${table.name}`, done => {
        fetch.mockResponse(fixtures[table.name].get);
        SDK[table.name].get().then(result => {
          expect(JSON.stringify(result)).toEqual(fixtures[table.name].get);
          done();
        });
      });
    }

    if (
      !table.hasOwnProperty("crudMethods") ||
      table.crudMethods.includes("find")
    ) {
      it(`Find method: find one specific item by ID ${table.name}`, done => {
        fetch.mockResponse(fixtures[table.name].find);
        const id = JSON.parse(fixtures[table.name].find)[table.idKey || "id"];
        SDK[table.name].find(id).then(result => {
          expect(JSON.stringify(result)).toEqual(fixtures[table.name].find);
          done();
        });
      });
    }

    let createdItem = null;

    if (
      !table.hasOwnProperty("crudMethods") ||
      table.crudMethods.includes("create")
    ) {
      it(`Create method: add new item to the table ${table.name}`, done => {
        fetch.mockResponse(fixtures[table.name].create);
        SDK[table.name].create(table.createData).then(result => {
          createdItem = result.resource[0];
          expect(JSON.stringify(result)).toEqual(fixtures[table.name].create);
          done();
        });
      });
    }

    if (
      !table.hasOwnProperty("crudMethods") ||
      table.crudMethods.includes("update")
    ) {
      it(`Update method: update item of table ${table.name}`, done => {
        fetch.mockResponse(fixtures[table.name].update);
        SDK[table.name]
          .update(createdItem[table.idKey], table.updateData)
          .then(result => {
            expect(JSON.stringify(result)).toEqual(fixtures[table.name].update);
            done();
          });
      });
    }

    if (
      !table.hasOwnProperty("crudMethods") ||
      table.crudMethods.includes("delete")
    ) {
      it(`Delete method: delete an item of the table ${table.name}`, done => {
        fetch.mockResponse(fixtures[table.name].delete);
        SDK[table.name].delete(createdItem[table.idKey]).then(result => {
          expect(JSON.stringify(result)).toEqual(fixtures[table.name].delete);
          done();
        });
      });
    }
  });
};
