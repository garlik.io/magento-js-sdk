import _ from 'lodash';

/**
 * Magento Client
 *
 * @class
 * @returns {Promise} Return a promise object
 */
// eslint-disable-next-line import/prefer-default-export
export default class MagentoClient {
  /**
   * @member {AxiosInstance} connection
   */
  connection

  /**
   * Constructor description
   *
   * @param {AxiosInstance} magentoConnection MagentoConnection should be passed
   * @param {string} defaultStore Determines which store to default to when requesting data.
   */
  constructor (magentoConnection, defaultStore = 'all') {
    this.connection = magentoConnection;
    this.defaultStore = defaultStore;
  }

  /**
   * Make a get request to Magento
   *
   * @param {string} path
   * @param {string=} store Optionally Override Store
   * @param {AxiosRequestConfig=} axiosRequestConfig Optionally add Axios configuration for request
   */
  get (path, store, axiosRequestConfig) {
    let currentStore = store;
    if (!_.isString(store)) {
      currentStore = this.defaultStore;
    }

    const requestUrl = MagentoClient.GetMagentoRequestPath(currentStore, path);

    return this.connection.get(requestUrl, axiosRequestConfig);
  }

  /**
   * Get Magento Request URI
   *
   * @param storeCode
   * @param uriPath
   *
   * @throws TypeError
   * @returns {string}
   */
  static GetMagentoRequestPath (storeCode, uriPath) {
    if (!_.isString(storeCode) || !_.isString(uriPath)) {
      throw new TypeError('storeCode and uriPath must both be provided.');
    }

    return `${storeCode}/${_.trimStart(uriPath, '/')}`;
  }

  /**
   * Get full magento request URL
   *
   * @param storeCode
   * @param uriPath
   *
   * @throws TypeError
   * @returns {string}
   */
  getFullMagentoRequestUrl (storeCode, uriPath) {
    if (!_.isString(storeCode) || !_.isString(uriPath)) {
      throw new TypeError('storeCode and uriPath must both be provided.');
    }

    return `${this.connection.defaults.baseURL}` +
      `/${MagentoClient.GetMagentoRequestPath(storeCode, uriPath)}`;
  }

  /**
   * Get full magento request URL
   *
   * @param storeCode
   *
   * @throws TypeError
   * @returns {MagentoClient}
   */
  setDefaultStore (storeCode) {
    if (!_.isString(storeCode)) {
      throw new TypeError('storeCode');
    }

    this.defaultStore = storeCode;

    return this;
  }
}
