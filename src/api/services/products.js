export default {
  channels: {
    crudMethods: ['get'],
    path: '/products/channels',
  },
  filters: {
    crudMethods: ['get'],
    path: '/products/filters',
  },
  images: {
    crudMethods: ['get'],
    path: '/products/images',
  },
  productCategories: {
    crudMethods: ['get'],
    path: '/products/categories',
  },
  productHasCategories: {
    crudMethods: ['get'],
    mock: 'products/product_category',
    path: 'products/_table/product_category',
  },
  products: {
    crudMethods: ['get'],
    path: '/products/product',
  },
  salespersons: {
    crudMethods: ['find', 'get'],
    path: '/products/salespeople',
  },
  salespersonsStates: {
    crudMethods: ['find', 'get'],
    path: '/products/states',
  },
};
