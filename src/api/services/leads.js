const companiesModule = {
  companies: {
    path: '/leads/companies',
  },
};

const documentsModule = {
  documents: {
    path: '/leads/documents',
  },
  documentsTags: {
    path: '/leads/document-tags',
  },
};

const infoModule = {
  info: {
    path: '/leads/info',
  },
};

const leadsModule = {
  leads: {
    path: '/leads/leads',
  },
  leadsSurvey: {
    path: '/leads/lead-surveys',
  },
  leadsTypes: {
    path: '/leads/lead-types',
  },
};

const ordersModule = {
  orderItems: {
    path: '/leads/order-items',
  },
  orders: {
    path: '/leads/orders',
  },
};

const locationsModule = {
  countries: {
    path: '/leads/countries',
  },
  locations: {
    path: '/leads/locations',
  },
  states: {
    path: '/leads/states',
  },
};

const samplesModule = {
  samples: {
    path: '/leads/samples',
  },
};

const surveysModule = {
  answers: {
    path: '/leads/answers',
  },
  questionOptions: {
    path: '/leads/question-options',
  },
  questions: {
    path: '/leads/questions',
  },
  surveys: {
    path: '/leads/surveys',
  },
};

const tradeshowsModule = {
  tradeshows: {
    path: '/leads/tradeshows',
  },
  tradeshowsCategories: {
    path: '/leads/tradeshow-categories',
  },
  tradeshowsSync: {
    path: '/leads/tradeshow-syncs',
  },
  tradeshowsTypes: {
    path: '/leads/tradeshow-types',
  },
};

const usersModule = {
  users: {
    path: '/leads/users',
  },
  usersTypes: {
    path: '/leads/user-types',
  },
};

const verificationCodesModule = {
  getUserCode: {
    path: '/verification/get-user-code',
  },
  verifyCompanyCode: {
    path: '/verification/company-code',
  },
  verifyUserCode: {
    path: '/leads/user-types',
  },
};

export default {
  ...companiesModule,
  ...documentsModule,
  ...infoModule,
  ...leadsModule,
  ...locationsModule,
  ...ordersModule,
  ...samplesModule,
  ...surveysModule,
  ...tradeshowsModule,
  ...usersModule,
  ...verificationCodesModule,
};
