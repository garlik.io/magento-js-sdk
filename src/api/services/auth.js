export default {
  jwt: {
    crudMethods: ['create'],
    path: '/auth/jwt',
  },
};
