import auth from './services/auth';
import leads from './services/leads';
import mail from './services/mail';
import products from './services/products';
import recipes from './services/recipes';
import sms from './services/sms';

export default {
  ...auth,
  ...leads,
  ...mail,
  ...products,
  ...recipes,
  ...sms,
};
