import productsModule from './productsModule';
import salesforceModule from './salesforceModule';

export default {
  productsModule,
  salesforceModule,
};
