let referenceToThis = null;

const endpoint = '/salesforce/sobjects';

const create = (sobject, data = {}) => {
  return new Promise((resolve, reject) => {
    referenceToThis.HttpClient.post(`${endpoint}/${sobject}`, data)
      .then((result) => {
        return resolve(result);
      })
      .catch((error) => {
        return reject(error);
      });
  });
};

const createMany = (sobject, data = {}) => {
  return new Promise((resolve, reject) => {
    referenceToThis.HttpClient.post(`${endpoint}/${sobject}/bulk`, data)
      .then((result) => {
        return resolve(result);
      })
      .catch((error) => {
        return reject(error);
      });
  });
};

const salesforceModule = (thisObject) => {
  referenceToThis = thisObject;
  referenceToThis.salesforce = {
    sobjects: {
      create,
      createMany,
    },
  };
};

export default salesforceModule;
