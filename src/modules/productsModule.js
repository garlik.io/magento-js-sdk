let referenceToThis = null;

/**
 * This function is responsible to set a tradeshow
 *
 * @param {object} params - Object with key equivalent to a column of the table and value equivalent to the value or array of
 * value you want to filter
 * @returns {Promise} Returns a new Promise object.
 */
const filter = (params = {}) => {
  const keys = Object.keys(params);

  return new Promise((resolve, reject) => {
    referenceToThis.products
      .get()
      .then((result) => {
        const filteredResult = result.resource.filter((item) => {
          let verify = true;
          keys.forEach((key) => {
            if (verify) {
              if (typeof params[key] === 'object') {
                verify = params[key].includes(item[key]);
              } else {
                verify = params[key] === item[key];
              }
            }
          });

          return verify;
        });
        result.resource = filteredResult;

        return resolve(result);
      })
      .catch((error) => {
        return reject(error);
      });
  });
};

const productsModule = (thisObject) => {
  referenceToThis = thisObject;
  referenceToThis.products.filter = filter;
};

export default productsModule;
