import axios from 'axios';

/**
 * Magento API Key
 *
 * @class
 * @returns null
 */
export default class MagentoConnection {
  /**
   * Generate API key via admin username & password
   *
   * @static
   * @param {string} magentoBaseUrl - Magento admin credentials
   *
   * @returns AxiosInstance
   */
  static getRawInstance (magentoBaseUrl) {
    return axios.create({
      baseURL: `${magentoBaseUrl}/rest/`,
      headers: {'Content-Type': 'application/json'},
      timeout: 10000,
    });
  }

  /**
   * Generate Magento (Axios) connection instance from an admin account
   *
   * @static
   * @param {object} credentials - Magento admin credentials
   * @param {string} credentials.username - The name of the employee.
   * @param {string} credentials.password - The name of the employee.
   * @param {string} magentoBaseUrl - The employee's department.
   *
   * @throws AxiosError
   * @returns AxiosInstance
   */
  static FromAdminAccount = async function ({username, password}, magentoBaseUrl) {
    const adminTokenPath = '/all/V1/integration/admin/token';
    const instance = MagentoConnection.getRawInstance(magentoBaseUrl);

    const {data: token} = await instance.post(adminTokenPath, {
      password,
      username,
    });

    instance.defaults.headers.common.authorization = `Bearer ${token}`;

    return instance;
  }
}

