const path = require('path');
const _ = require('lodash');
const env = require('yargs').argv.env;
const libraryName = 'magento';

let outputFile, mode;

if (env === 'build') {
  mode = 'production';
  outputFile = libraryName + '.min.js';
} else {
  mode = 'development';
  outputFile = libraryName + '.js';
}

const defaultConfig = {
  mode: mode,
  entry: __dirname + '/src/index.js',
  devtool: 'inline-source-map',
  output: {
    path: __dirname + '/dist',
    filename: outputFile,
    library: libraryName,
    libraryTarget: 'umd',
    umdNamedDefine: true,
    globalObject: 'typeof self !== \'undefined\' ? self : this'
  },
  module: {
    rules: [
      {
        test: /(\.jsx|\.js)$/,
        loader: 'babel-loader',
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /(\.jsx|\.js)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    modules: [path.resolve('./node_modules'), path.resolve('./src')],
    extensions: ['.json', '.js']
  }
};

const webConfig = _.cloneDeep(defaultConfig);
webConfig.target = 'web';
webConfig.output.filename = 'web.' + webConfig.output.filename;

const serverConfig = _.cloneDeep(defaultConfig);
serverConfig.target = 'node';
serverConfig.output.filename = 'node.' + serverConfig.output.filename;

module.exports = [webConfig, serverConfig];
